# A history of the web
## A study about the web, it's original ambitions and why decentralization matters

+++

# Motivations

- Study about the web history and ambitions
- Document that study with references
- Present it in an easily consumable way
- Allow for collaboration

---

# Before the web

---

# Incipient web

---

# Early days

---

# Walled gardens & other failures

---

# Activism & Advocacy
## Decentralization, web literacy & other initiatives

+++

# Decentralization
## Topics to cover

- [ ] Scuttlebutt
- [ ] Dat
- [ ] IPFS
- [ ] Blockchain
